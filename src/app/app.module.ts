import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AcceptConnectionComponent } from './accept-connection/accept-connection.component';
import {WebRTCService} from './Services/web-r-t-c.service';
import { QRCodeModule } from 'angular2-qrcode';

@NgModule({
  declarations: [
    AppComponent,
    AcceptConnectionComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    QRCodeModule
  ],
  providers: [WebRTCService],
  bootstrap: [AppComponent]
})
export class AppModule { }
