import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

/*
const ICE_SERVERS: RTCIceServer[] = [
  {urls: ['stun:stun.example.com', 'stun:stun-1.example.com']},
  {urls: 'stun:stun.l.google.com:19302'}
];

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
  iceServers: ICE_SERVERS
};
*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(fb: FormBuilder) {

  }

  ngOnInit(): void {

  }

  /*
  @ViewChild('dataChannelSend') dataChannelSend: ElementRef;
  clientCode;

  textareas: FormGroup;


  private peerConnection: RTCPeerConnection;
  private signalingConnection: WebSocket;
  private sendChannel: RTCDataChannel;
  private uuid;

  private dest;

  constructor(fb: FormBuilder) {
    this.textareas = fb.group({
      dataChannelSend: new FormControl({value: '', disabled: true}),
      dataChannelReceive: ['']
    });
  }

  ngOnInit(): void {
    this.dataChannelSend.nativeElement.placeholder = 'Press Start, enter some text, then press Send...';
    this.uuid = this.createUuid();
  }

  connect() {

    this.setupSignalingServer();
    this.signalingConnection.onopen =()=> this.signalingConnection.send(JSON.stringify({ 'type': "login", 'data': this.uuid }));

    this.setupPeerServer();

  }

  start() {
    this.peerConnection
      .createOffer()
      .then(this.setDescription())
      .catch(this.errorHandler);
  }


  send() {
    this.sendChannel.send('hi');
  }






  private setupSignalingServer() {
    this.signalingConnection = new WebSocket(`ws://${window.location.hostname}:8181/signal`);
    this.signalingConnection.onmessage = this.getSignalMessageCallback();
    this.signalingConnection.onerror = this.errorHandler;
  }

  private setupPeerServer() {

    let handleDataChannelOpen = function (event) {
      console.log("dataChannel.OnOpen", event);
    };

    let handleDataChannelMessageReceived = function (event) {
      console.log("dataChannel.OnMessage:", event);
    };

    let handleDataChannelError = function (error) {
      console.log("dataChannel.OnError:", error);
    };

    let handleDataChannelClose = function (event) {
      console.log("dataChannel.OnClose", event);
    };

    let handleChannelCallback = function (event) {
      this.sendChannel = event.channel;
      this.sendChannel.onopen = handleDataChannelOpen;
      this.sendChannel.onmessage = handleDataChannelMessageReceived;
      this.sendChannel.onerror = handleDataChannelError;
      this.sendChannel.onclose = handleDataChannelClose;
    };

    this.peerConnection = new RTCPeerConnection(PEER_CONNECTION_CONFIG);
    this.peerConnection.onicecandidate = this.getIceCandidateCallback();
    this.peerConnection.ondatachannel = handleChannelCallback;
    this.sendChannel = this.peerConnection.createDataChannel('dataChannelName', {});

    this.sendChannel.onopen = handleDataChannelOpen;
    this.sendChannel.onmessage = handleDataChannelMessageReceived;
    this.sendChannel.onerror = handleDataChannelError;
    this.sendChannel.onclose = handleDataChannelClose;
  }

  private getSignalMessageCallback(): (string) => void {
    return (message) => {
      const signal = JSON.parse(message.data);
      if (signal.dest === this.uuid) {
        return;
      }

      console.log('Received signal');
      console.log(signal);

      if (signal.sdp) {
        this.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp))
          .then(() => {
            console.log("remote Desc SET");
            if (signal.sdp.type === 'offer') {
              this.dest=signal.data;
              if(this.dest) {
                this.clientCode = this.dest;
              }
              this.peerConnection.createAnswer()
                .then(this.setDescription2())
                .catch(this.errorHandler);
            }
          })
          .catch(this.errorHandler);
      } else if (signal.ice) {
        console.log("adding Ice");
        console.log(signal.ice);
        this.peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(this.errorHandler);
      }
    };
  }

  private getIceCandidateCallback(): (string) => void {
    return (event) => {
      console.log(`ice candidate:`);
      console.log(event);

      if (event.candidate != null) {
        this.signalingConnection.send(JSON.stringify({ 'type':'rtc','ice': event.candidate, 'dest': this.clientCode }));
      }
    };
  }

  private setDescription(): (string) => void {
    return (description) => {
      console.log('got description');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          console.log(this.clientCode)
          this.signalingConnection.send(JSON.stringify({  'sdp': this.peerConnection.localDescription, 'type':"rtc",'dest': this.clientCode,'data':this.uuid}));
        })
        .catch(this.errorHandler);
    };
  }

  private setDescription2(): (string) => void {
    return (description) => {
      console.log('got description2');
      console.log(description);

      this.peerConnection.setLocalDescription(description)
        .then(() => {
          console.log(this.dest);
          this.signalingConnection.send(JSON.stringify({  'sdp': this.peerConnection.localDescription, 'type':"rtc",'dest': this.dest}));
        })
        .catch(this.errorHandler);
    };
  }


  private errorHandler(error) {
    console.log(error);
  }

  // Taken from http://stackoverflow.com/a/105074/515584
  // Strictly speaking, it's not a real UUID, but it gets the job done here
  private createUuid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }
  */
}
