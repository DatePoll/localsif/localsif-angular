import {Component, NgZone, OnInit, ChangeDetectorRef} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {WebRTCService} from '../Services/web-r-t-c.service';

const ICE_SERVERS: RTCIceServer[] = [
    {urls: ['stun:stun.example.com', 'stun:stun-1.example.com']},
    {urls: 'stun:stun.l.google.com:19302'}
];

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
    iceServers: ICE_SERVERS
};

@Component({
    selector: 'app-accept-connection',
    templateUrl: './accept-connection.component.html',
    styleUrls: ['./accept-connection.component.css']
})
export class AcceptConnectionComponent implements OnInit {


    constructor(private sanitizer: DomSanitizer,
                private ngZone: NgZone,
                private changeDetection: ChangeDetectorRef,
                public webRTCService: WebRTCService) {
    }


    connectToServer() {
        this.webRTCService.establishConnectionToServer();
    }

    connectToClient() {
        this.webRTCService.establishConnectionToClient();
    }

    sendFiles() {
        this.webRTCService.sendFiles();
    }


    removeFromUpload(file) {
        this.webRTCService.removeFromUpload(file);
    }
    onFileChange($event: Event) {
        this.webRTCService.filesToSendChanged(event.target);
    }

    ngOnInit(): void {
    }

}
