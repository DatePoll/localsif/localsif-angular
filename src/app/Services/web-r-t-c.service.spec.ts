import { TestBed } from '@angular/core/testing';

import { WebRTCService } from './web-r-t-c.service';

describe('WebRTCService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebRTCService = TestBed.get(WebRTCService);
    expect(service).toBeTruthy();
  });
});
