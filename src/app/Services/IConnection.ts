interface IConnection {
    establishConnectionToServer();
    establishConnectionToClient();
    disconnect(client)
    sendFiles();
}