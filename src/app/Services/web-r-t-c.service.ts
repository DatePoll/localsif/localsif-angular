import {DomSanitizer} from '@angular/platform-browser';
import { NgZone} from '@angular/core';
import { Injectable } from '@angular/core';

const ICE_SERVERS: RTCIceServer[] = [
  {urls: ['stun:stun.example.com', 'stun:stun-1.example.com']},
  {urls: 'stun:stun.l.google.com:19302'}
];

const PEER_CONNECTION_CONFIG: RTCConfiguration = {
  iceServers: ICE_SERVERS
};

//TODO: Connection Interface( Connect() -> return IDataChannel,Disconnect())
//TODO: FileSenderService (+interface)(SendFile(), SendText(),Progress, ...)
//TODO: DataChannelInterface(Konkret WebRTCDataChannel mit Send/Use Methode)

@Injectable()
export class WebRTCService implements IConnection {

  constructor(private sanitizer: DomSanitizer, private ngZone: NgZone) {
    this.createUUID();
  }

  // region Connection Booleans
  EstablishedConnectionWithServer = false;
  UploadedFileToSend = false;
  EstablishedConnectionWithClient = false;
  CreatedSendChannel = false;

  TriedToConnectWithServer=false;

  connectionWaitingforAccept = false;

  acceptButton;
  declineButton;

  connectionAccepted = false;
  // endregion

  // region Datatranfer Details
  receivingFileName;
  receivingFileSize;

  bitrateDiv;
  filetoSend;

  downloadAnchor;
  downloadAnchorDownload = '';
  downloadAnchorHref;
  downloadAnchorTextContent = '';
  downloadAnchorStyleDisplay = '';


  sendProgressMax = 1;
  sendProgressValue = 0;
  receiveProgressMax = 1;
  receiveProgressValue = 0;
  statusMessage;


  receiveBuffer: Blob[] = [];
  receivedSize = 0;
  // endregion

  // region Datatranfer Data
  filesToSend = [];
  receivedFiles = [];
  // endregion

  // region connections
  private peerConnection: RTCPeerConnection = new RTCPeerConnection();
  private peerConnections: { [key: string]: RTCPeerConnection } = {};

  private signalingConnection: WebSocket;


  peerUIDs: string[] = [];
  private sendChannels: { [key: string]: RTCDataChannel } = {};

  private receiveChannel: RTCDataChannel;
  // endregion

  // region client codes
  uuid;
  private dest;
  clientCode;
  // endregion


  // region sending one or more files
  private fileSendIndex = 0;
  peerIndex: string;

  createUUID(): void {
    this.uuid = this.createUuid();
  }

  sendFiles() {
    if (this.filesToSend.length !== 0
        && this.filesToSend[this.fileSendIndex] !== undefined
        && this.filesToSend[this.fileSendIndex] !== null) {
      console.log(this.filesToSend[this.fileSendIndex]);
      this.sendData(this.filesToSend[this.fileSendIndex]).then(() => {
        this.fileSendIndex++;
        this.sendFiles();
      });
    }
  }

  private waitFor(conditionFunction) {

    const poll = resolve => {
      if (conditionFunction()) {
        console.log('RESOLVED');
        resolve();
      } else {
        setTimeout(_ => poll(resolve), 400);
      }
    };

    return new Promise(poll);
  }

  private async sendData(file): Promise<void> {

    // region set file and send info
    this.statusMessage = '';
    this.downloadAnchor = '';
    if (file.size === 0) {
      this.bitrateDiv.innerHTML = '';
      this.statusMessage = 'File is empty, please select a non-empty file';
      return;
    }

    let chunkCounter = 0;
    console.log('CLIENTCOUNT:' + this.peerUIDs.length);

    this.sendProgressMax = file.size;
    // this.receiveProgress.max = file.size;
    const chunkSize = 16384;
    const fileReader: any = new FileReader();
    let offset = 0;
    // endregion

    // region defining fileReader events and needed functions
    fileReader.addEventListener('error', error => console.error('Error reading file:', error));
    fileReader.addEventListener('abort', event => console.log('File reading aborted:', event));
    fileReader.addEventListener('load', e => {

      let peercnt = 0;
      const res = (e.target.result);
      for (const chanUID of this.peerUIDs) {               // itterating all peers through
        console.log('FileRead.onload ', e);
        peercnt++;


        // region checking if the first part of the file got loaded => send file info
        if (offset === 0 || peercnt <= this.peerUIDs.length) {
          console.log('sending file info');
          console.log(`File is ${[file.name, file.size, file.type, file.lastModified].join(' ')}`);
          this.sendChannels[chanUID].send('info;' + file.name + ';' + file.size + ';' + file.type);
        }
        // endregion


        console.log('sending file data');

        // region send file chunk after chunk

        this.sendChannels[chanUID].send(res);
        if (offset === 0 || (peercnt % this.peerUIDs.length) === 0) {
          offset += res.byteLength;
        }
        this.sendProgressValue = offset;
        if (offset < file.size) {
          readSlice(offset);
          chunkCounter++;
        } else {
        }
        // endregion

      }
    });
    // endregion

    // region loading next slice of the file
    const readSlice = o => {
      console.log('readSlice ', o);
      const slice = file.slice(offset, o + chunkSize);
      fileReader.readAsArrayBuffer(slice);
    };
    // endregion

    readSlice(0); // initing send process

    // region waiting for the file to be sent then return resolve
    await this.waitFor(() => {
      console.log('CHUNKCOUNTER ' + chunkCounter);
      console.log('FILESIZE ' + file.size);
      console.log('CHUNKSIZE ' + chunkSize);
      return chunkCounter === Math.floor(file.size / chunkSize);
    });
    return new Promise<void>(resolve => {
      console.log('RESOLVE SENDDATA');
      return resolve();
    });
    // endregion

  }
  // endregion

  onReceiveMessageCallback(event) {
    // region set info of received file
    if (event.data.toString().substr(0, 4) === 'info') {
      const array = event.data.split(';');
      this.receivingFileName = array[1];
      this.receivingFileSize = array[2];
      this.ngZone.run(() => this.receiveProgressMax = array[2]);

      console.log('Info Recieved:' + this.receivingFileName + ', ' + this.receivingFileSize);
    } else {
      this.downloadAnchorTextContent = '';
      this.downloadAnchorDownload = null;
      if (this.downloadAnchorHref) {
        URL.revokeObjectURL(this.downloadAnchorHref);
        this.downloadAnchorHref = null;
      }

      // region receive parts of file => put them together to a whole file
      console.log(`Received Message ${event}`);
      console.log(event);
      this.receiveBuffer.push(event.data);
      // | because firefox event.data is blob and chrome event.data is ArrayBuffer
      // tslint:disable-next-line:no-bitwise
      this.receivedSize += event.data.byteLength | event.data.size;

      this.receiveProgressValue = this.receivedSize;
      console.log('BAR:' + this.receiveProgressValue);
      console.log(this.receivedSize + ' already recieved, expected ' + this.receivingFileSize);
      // endregion

      // region if file is fully recieved => create downloadlink and set other infos
      if (this.receivedSize == this.receivingFileSize) {

        this.receivedSize = 0;
        console.log("FILE RECEIVED SUCCESSFULLY");
        const received = new Blob(this.receiveBuffer);
        this.receiveBuffer = [];

        const link: any = {
          href: this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(received)),
          download: this.receivingFileName,
          textContent: `download \'${this.receivingFileName}\' (${this.receivingFileSize} bytes)`
        };

        this.receivedFiles.push(link);


        this.downloadAnchorHref = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(received));
        this.ngZone.run(() => {
              this.downloadAnchorDownload = this.receivingFileName;
              this.downloadAnchorTextContent =
                  `Click to download '${this.receivingFileName}' (${this.receivingFileSize} bytes)`;
              this.downloadAnchorStyleDisplay = 'display:block;';
              // let bitrate = Math.round(this.receivedSize * 8 /
              //  ((new Date()).getTime() - this.timestampStart));
              // this.bitrateDivInnerHTML
              //  = `<strong>Average Bitrate:</strong> ${bitrate} kbits/sec (max: ${this.bitrateMax} kbits/sec)`;

            }
        );
      }
      // endregion
    }
  }


  // region ConnectionHandling
  disconnect(client) {
    console.log('Closing data channels');
    if (this.sendChannels[client]) {
      this.sendChannels[client].close();
    }
    console.log(`Closed data channel with label: ${this.sendChannels[client].label}`);

    if (this.receiveChannel) {
      this.receiveChannel.close();
      console.log(`Closed data channel with label: ${this.receiveChannel.label}`);
    }

    if (this.peerConnections[client]) {
      this.peerConnections[client].close();
      this.peerConnections[client] = null;
    }

    const index: number = this.peerUIDs.indexOf(client);
    if (index !== -1) {
      this.peerUIDs.splice(index, 1);
    }
    if (this.peerUIDs.length === 0) {

      this.EstablishedConnectionWithClient = false;
      this.CreatedSendChannel = false;

    }

    console.log('Closed peer connections');

    // re-enable the file select
    // this.filetoSend.disabled = false;
    // this.abortButton.disabled = true;
    // this.sendFileButton.disabled = false;


  }


  establishConnectionToServer() {
    this.setupSignalingConnection();
  }

  establishConnectionToClient() {
    if (this.clientCode !== this.uuid) {
      this.peerUIDs.push(this.clientCode);
      this.peerIndex = this.clientCode;

      this.peerConnections[this.peerIndex] = new RTCPeerConnection();

      this.setupPeerConnection();

      this.signalingConnection.onmessage = this.getSignalMessageCallback();

      this.peerConnections[this.peerIndex]
          .createOffer()
          .then(this.setDescription())
          .catch(this.errorHandler);

    } else {
      console.log('cant connect to yourself');
    }


  }

  private setupSignalingConnection() {
    // Config Signaling Connection
    this.signalingConnection = new WebSocket(`ws://${window.location.hostname}:8181/signal`);
    this.signalingConnection.onmessage = this.getSignalMessageCallback();
    this.signalingConnection.onerror = this.errorHandler;
    this.signalingConnection.onopen = () => this.signalingConnection.send(JSON.stringify({
      'type': 'login',
      'data': this.uuid
    }));
    this.TriedToConnectWithServer=true;
  }

  private setupPeerConnection() {

    // Config Peer Connecitons
    this.peerConnections[this.peerIndex] = new RTCPeerConnection(PEER_CONNECTION_CONFIG);
    this.peerConnections[this.peerIndex].onicecandidate = this.getIceCandidateCallback();
    this.peerConnections[this.peerIndex].ondatachannel = (e) => {
      this.sendChannels[this.peerIndex] = e.channel;
      this.sendChannels[this.peerIndex].onmessage = this.onReceiveMessageCallback;
    };

    // Config DataChannels
    console.log('creating DataChannel ' + this.peerIndex + ' for ' + this.peerConnections[this.peerIndex].getStats());
    this.sendChannels[this.peerIndex] = this.peerConnections[this.peerIndex].createDataChannel(this.peerIndex.toString(), {});
    this.sendChannels[this.peerIndex].onopen = (e) => {
      this.CreatedSendChannel = true;
      console.log('opened SendChannel');
    };
    this.sendChannels[this.peerIndex].onmessage = (e) => this.onReceiveMessageCallback(e);
    this.sendChannels[this.peerIndex].onerror = (e) => console.log('error SendChannel');
    this.sendChannels[this.peerIndex].onclose = (e) => {
      this.disconnect((<RTCDataChannel>e.target).label);

      /*
      const index: number = this.peerUIDs.indexOf((<RTCDataChannel>e.target).label);
      console.log(index);
      if (index !== -1) {
        this.peerUIDs.splice(index, 1);
      }
      if(this.peerUIDs.length==0)
      {
        this.SendButtonFile=false;
      }
      this.changeDetection.detectChanges();
      */
      console.log('closed SendChannel');
    };
  }


  // wait for button input to accept or decline connection request
  async dialogButtonPress(): Promise<boolean> {
    this.acceptButton = document.getElementById('accept-button');
    this.declineButton = document.getElementById('decline-button');
    return new Promise<boolean>((resolve) => {
      const resolver = (ev: Event) => {
        this.acceptButton.removeEventListener('click', resolver);
        this.declineButton.removeEventListener('click', resolver);
        // This is where we determine which button was clicked.
        resolve(ev.target === this.acceptButton ? true : false);
      };
      this.acceptButton.addEventListener('click', resolver);
      this.declineButton.addEventListener('click', resolver);
    });
  }

  private sdpOffer(signal)
  {
    this.connectionWaitingforAccept = true;
    // wait on button input
    this.dialogButtonPress().then(x => { // TODO Change, maybe whole method async
      this.connectionWaitingforAccept = false;
      if (x) {
        // only executed from the client which is connected to, adds the client that connects to peerConnections
        if (signal.data && !this.peerConnections[signal.data] && signal.type.toString() !== 'Server') {
          console.log('Setting peer dings');
          this.peerUIDs.push(signal.data);
          this.peerConnections[signal.data] = this.peerConnection;
          this.peerIndex = signal.data;
          this.setupPeerConnection();
        }

        this.peerConnections[this.peerIndex].setRemoteDescription(new RTCSessionDescription(signal.sdp))
            .then(() => {
              console.log('remote Desc SET');
              console.log('TYPE:' + signal.sdp.type);
              if (signal.sdp.type.toLowerCase() === 'offer') {
                this.dest = signal.data;
                if (this.dest) {
                  this.peerIndex = this.dest;
                  this.clientCode = '';
                }
                this.peerConnections[this.peerIndex].createAnswer()
                    .then(this.setDescription())
                    .catch(this.errorHandler);

                this.EstablishedConnectionWithClient = true;

              }
            })
            .catch(this.errorHandler);
        this.connectionAccepted = true;
      } else {
        // TODO: peerconnection löschen

      }
    });
  }

  private sdpAnswer(signal)
  {
    this.peerConnections[this.peerIndex].setRemoteDescription(new RTCSessionDescription(signal.sdp))
        .then(() => {
          console.log('remote Desc SET');
          this.clientCode = '';
          this.EstablishedConnectionWithClient = true;
          this.connectionAccepted = true;
        });
  }

  private ice(signal)
  {
    this.waitFor(() => this.connectionAccepted === true && this.peerConnections[this.peerIndex].remoteDescription)
        .then(() => {
          console.log('adding Ice');
          console.log(signal.ice);
          // calls getIceCandidateCallback() on the other client
          this.peerConnections[this.peerIndex].addIceCandidate(new RTCIceCandidate(signal.ice)).catch(this.errorHandler);
        });
  }

  private getSignalMessageCallback(): (string) => void {
    return (message) => {
      const signal = JSON.parse(message.data);

      // Check if message is from Server
      if (!this.clientCode && signal.type.toString().toLowerCase() !== 'server') {
        this.clientCode = signal.data;
      }

      // Check if Message is for this Client
      if (signal.dest === this.uuid) {
        console.log('Received signal');
        console.log(signal);

        if (signal.sdp) {
          console.log('TYPE:' + signal.sdp.type.toLowerCase());
        }
        if (signal.sdp && signal.sdp.type.toLowerCase() === 'offer') {
          this.sdpOffer(signal);
        } else if (signal.sdp && signal.sdp.type.toLowerCase() === 'answer') {
          this.sdpAnswer(signal);
        } else if (signal.ice) {
          this.ice(signal);
        } else if (signal.type.toString().toLowerCase() === 'server') {
          if (signal.data.toString() === 'ACCEPTED') {
            this.EstablishedConnectionWithServer = true;
          } else {
            this.EstablishedConnectionWithServer = false;
          }
        }
      } else {
        console.log('Message not for this client');
        console.log(signal);
      }
    };
  }

  private getIceCandidateCallback(): (string) => void {
    return (event) => {
      console.log(`ice candidate:`);
      console.log(event);
      if (event.candidate != null) {
        this.signalingConnection.send(JSON.stringify({
          'type': 'rtc',
          'ice': event.candidate,
          'dest': this.peerIndex
        }));
      }
    };
  }

  private setDescription(): (string) => void {
    return (description) => {
      console.log('got description');
      console.log(description);
      console.log(this.peerIndex);

      this.peerConnections[this.peerIndex].setLocalDescription(description)
          .then(() => {
            console.log(this.peerIndex);
            this.signalingConnection.send(JSON.stringify({
              'sdp': this.peerConnections[this.peerIndex].localDescription,
              'type': 'rtc',
              'dest': this.peerIndex,
              'data': this.uuid
            }));
          })
          .catch(this.errorHandler);
    };
  }

  /*
  private setDescription2(): (string) => void {
    return (description) => {
      console.log('got description2');
      console.log(description);

      this.peerConnections[this.peerIndex].setLocalDescription(description)
          .then(() => {
            console.log(this.peerIndex);
            this.signalingConnection.send(JSON.stringify({
              'sdp': this.peerConnections[this.peerIndex].localDescription,
              'type': 'rtc',
              'dest': this.peerIndex,
              'data': this.uuid
            }));
          })
          .catch(this.errorHandler);
    };
  }
   */


  private errorHandler(error) {
    console.log(error);
  }

  // Taken from http://stackoverflow.com/a/105074/515584
  // Strictly speaking, it's not a real UUID, but it gets the job done here
  private createUuid(): string {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  // endregion
  removeFromUpload(file) {
    const index: number = this.filesToSend.indexOf(file);
    if (index !== -1) {
      this.filesToSend.splice(index, 1);
    }
    if (this.filesToSend.length === 0) {
      this.UploadedFileToSend = false;
    }
  }

  filesToSendChanged(file) {
    this.filetoSend = file;
    this.filesToSend.push(this.filetoSend.files[0]);
    console.log(this.filesToSend);
    if (this.filetoSend) {
      this.UploadedFileToSend = true;
    }
    console.log(event);
    this.fileSendIndex = 0;
  }

}
